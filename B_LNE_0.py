from shapely.geometry import Point
from shapely.geometry import LineString
from shapely.geometry import MultiPoint

def line_intersect_circle(l, c, three_d=False):
    """returns number of intersections and list of intersection coordinates\n
        line information passed as [x1,y1,x1,y2]\n
        circle information passed as [x,y,r]\n
        assumes lines have length > 0 and circles have radius > 0"""

    p1 = (l[0], l[1])
    p2 = (l[2], l[3])
    line = LineString([p1, p2])
    circle = Point(c[0],c[1]).buffer(c[2]).boundary

    intersection = circle.intersection(line)

    if isinstance(intersection, MultiPoint):
        return 2, [(p.x, p.y) for p in intersection]

    elif isinstance(intersection, Point):
        return 1, [(intersection.x, intersection.y)]
        
    return 0, []
