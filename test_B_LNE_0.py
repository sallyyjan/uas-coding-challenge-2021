import unittest
from B_LNE_0 import line_intersect_circle as intersect

class IntersectTestCases(unittest.TestCase):
    """Test cases for line_intersct_circle"""

    def test_case_0_num_intersects(self):
        l = [0,0,1,0]
        c = [0,0,1]
        num_intersects, coord = intersect(l,c)
        self.assertEqual(num_intersects, 1)
        self.assertEqual(coord, [(1,0)])

    def test_case_1_num_intersects(self):
        l = [0,10,30,10]
        c = [12,0,10]
        num_intersects, coord = intersect(l,c)
        self.assertEqual(num_intersects, 1)
        self.assertAlmostEqual(coord[0][0], 12)
        self.assertAlmostEqual(coord[0][1], 10)

    def test_case_2_num_intersects(self):
        l = [0,-10,15,15]
        c = [9,3,5] 
        num_intersects, coord = intersect(l,c)
        self.assertEqual(num_intersects, 2)
        self.assertAlmostEqual(coord[0][0], 5.6002336557, places=1)
        self.assertAlmostEqual(coord[0][1], -0.6662772405, places=1)
        self.assertAlmostEqual(coord[1][0], 10.6350604619, places=1)
        self.assertAlmostEqual(coord[1][1], 7.7251007699, places=1)


    def test_case_3_num_intersects(self):
        l = [0,-10,15,15]
        c = [10,-5,4] 
        num_intersects, coord = intersect(l,c)
        self.assertEqual(num_intersects, 0)
        self.assertEqual(coord, [])

if __name__ == '__main__':
    unittest.main()
