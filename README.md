# UAS-Coding-Challenge-2021

B-LNE-0 challenge for UBC UAS software challenge.

A function which takes in two lists describing a line and a circle as
[x1,y1,x2,y2] and [x,y,radius], respectively, returns the number of 
times the line intersects the circle, and the coordinates of the 
intersection(s) as a list of tuples.

Example:


`line = [0,0,1,0]`


`circle = [0,0,1]`


`num_intersects, coord = line_intersect_circle(l,c)`


`# num_intersects = 1`


`# coord =  [(1,0)]`


Used Shapely 1.7.1 on Python3 3.9.6
